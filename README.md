## KYSS secret bulk update

This module is a modest script that should allow bulk update of secret key values on KYSS.

### Usage
```shell
usage: kyss_secret_generator.py [-h] --url URL --mount-point MOUNT_POINT --path PATH [--conf CONF]

options:
  -h, --help            show this help message and exit
  --url URL             kyss url
  --mount-point MOUNT_POINT
                        mount point
  --path PATH           secret path
  --conf CONF           config file
```