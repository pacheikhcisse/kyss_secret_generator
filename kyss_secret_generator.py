import argparse
import os
import hvac
import toml
from password_generator import PasswordGenerator


def get_secrets(path, mount_point, client):
    """ Use the provided vault token to return the secrets in a given path
    :param path: The path where to check the secrets
    :param mount_point: The mount point
    :param client: The hvac client to use to authenticate to API
    """
    secrets = client.secrets.kv.list_secrets(path=path, mount_point=mount_point)['data']['keys']
    print("Secrets to update in path '%s/%s': %s" % (mount_point, path, secrets))
    return secrets


def generate_pwd(path, mount_point, client, secret, pwd_gen):
    kv_json = client.secrets.kv.read_secret(path="%s/%s" % (path, secret), mount_point=mount_point)['data']
    for item in kv_json['data']:
        kv_json['data'][item] = pwd_gen.generate()
    client.secrets.kv.create_or_update_secret(path="%s/%s" % (path, secret), mount_point=mount_point,
                                                  secret=dict(kv_json['data']))
    print("Keys updated for secret '%s': [%s]" % (secret, list(kv_json['data'].keys())))


def init_pwd_generator_config(config_file):
    # read pwd config file

    data = toml.load(config_file)
    pwd_config = data.get("PWD")
    print("Password configuration: %s" % pwd_config)

    pgen = PasswordGenerator()
    pgen.minlen = pwd_config.get("min_length")
    pgen.maxlen = pwd_config.get("max_length")
    pgen._schars = pwd_config.get("special_chars")
    return pgen


if __name__ == "__main__":
    # command-line arguments
    argParser = argparse.ArgumentParser()
    argParser.add_argument("--url", help="kyss url", required=True)
    argParser.add_argument("--mount-point", help="mount point", required=True)
    argParser.add_argument("--path", help="secret path", required=True)
    argParser.add_argument("--conf", help="config file", default="./config.toml")
    args = argParser.parse_args()

    # init hvac client
    client = hvac.Client(
        url=args.url,
        token=os.environ['VAULT_TOKEN'],
        verify=False  # TODO fix insecure mode
    )

    if client.is_authenticated():
        print("Authenticated to KYSS [url: %s]" % client.url)
        pgen = init_pwd_generator_config(args.conf)
        secrets = get_secrets(args.path, args.mount_point, client)
        for secret in secrets:
            generate_pwd(args.path, args.mount_point, client, secret, pgen)
    else:
        print("Authentication failed")
